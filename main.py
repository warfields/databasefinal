#!/usr/bin/python3

import getpass
import pg8000
from wordcloud import WordCloud
import matplotlib.pyplot as plt

username = getpass.getuser()
secret = getpass.getpass()
a = True
# If you entered the wrong password -> try again
while a:
    try:
        connection = pg8000.connect(user=username, password=secret, host='flowers.mines.edu', database='csci403')
        a = False
    except:
        print("Wrong password!")
        secret = getpass.getpass()

cursor = connection.cursor()

# immediate
# Execute select statement; acquire 10 random products
cursor.execute("SELECT product_title FROM aborghi.amazon_reviews WHERE star_rating=1 AND marketplace='US' ORDER BY random() LIMIT 10")

results = cursor.fetchall()

with open("common.txt", 'r') as words:
    common = []
    common.extend(words.readline().split())

# Select from each product acquired, all the "blank" star reviews
for row in results:

    # Prints the Product Title 
    product_title = row
    stmt = "SELECT review_body FROM aborghi.amazon_reviews WHERE star_rating=1 AND marketplace='US' AND product_title = %s"
    cursor.execute(stmt, product_title)
    results = cursor.fetchall()
    print(product_title)

    # Concatenate all "blank" star reviews into one string
    # Preparatation for entry into tags, which only accepts a string input
    text = ''
    for x in results:
        text += x[0]


    wordcloud = WordCloud(scale=10,stopwords=common).generate(text)
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis("off")
    product_title[0] += '.png'
    
    plt.savefig(product_title[0], dpi=300)

    # lower max_font_size
    """
    wordcloud = WordCloud(max_font_size=40).generate(text)
    plt.figure()
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    plt.show()
    """